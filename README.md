meme_parallel
=============

A Docker image with the [MEME suite](http://meme-suite.org/) installed with support for parallel processing through [OpenMPI](https://www.open-mpi.org/).

To run a meme job on 16 CPUs, you would run the following command:

```bash
docker run --rm -it --volume local_dir:/container_dir:rw mpirun -np 16 --allow-run-as-root \
	/meme/bin/meme <seq.fasta> [meme options]
```