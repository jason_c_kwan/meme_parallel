FROM ubuntu:16.04
MAINTAINER Jason C. Kwan "jason.kwan@wisc.edu"

RUN apt-get update
RUN apt-get install -y build-essential wget zlib1g-dev python curl libexpat1-dev libxml++2.6-dev libxslt1-dev openssh-server
RUN curl -L http://cpanmin.us | perl - App::cpanminus
RUN cpanm HTML::PullParser HTML::Template HTML::TreeBuilder JSON XML::Simple XML::Parser::Expat XML::Compile::SOAP11 XML::Compile::WSDL11 XML::Compile::Transport::SOAPHTTP Log::Log4perl Math::CDF
RUN wget https://www.open-mpi.org/software/ompi/v3.0/downloads/openmpi-3.0.1.tar.gz
RUN tar xvf openmpi-3.0.1.tar.gz
RUN cd openmpi-3.0.1 && ./configure --prefix=/openmpi && make all install
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/openmpi/lib"
RUN wget http://meme-suite.org/meme-software/4.12.0/meme_4.12.0.tar.gz
RUN tar xvf meme_4.12.0.tar.gz
COPY user.h /meme_4.12.0/src/
ENV PATH="/meme/bin:/openmpi/bin:${PATH}"
RUN cd meme_4.12.0 && ./configure --prefix=/meme --with-url="http://meme.ncbr.net/meme" --enable-openmp --enable-debug --with-mpicc=/openmpi/bin/mpicc --enable-opt && make && make install
